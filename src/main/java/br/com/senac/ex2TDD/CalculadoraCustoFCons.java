/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex2TDD;

/**
 *
 * @author sala304b
 */
public class CalculadoraCustoFCons {
    
    private final double PERCENTAGEM_DISTRIBUIDOR = 0.28 ; 
    private final double IMPOSTOS = 0.45;
    
    public double  calcular(double custo ){
        
        double imposto  = custo * IMPOSTOS ; 
        double margemLucro = custo * PERCENTAGEM_DISTRIBUIDOR ; 
        double precoFinal = custo + imposto + margemLucro ; 
        
        return  precoFinal ; 
    }
    
}
