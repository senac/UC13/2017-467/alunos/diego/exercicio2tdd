/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.ex2TDD.CalculadoraCustoFCons;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class CalculadoraCustoFConsTest {
    
    public CalculadoraCustoFConsTest() {
    }
    
    @Test
    public void deveCalcularCustoFinalConsumidor(){
        
        CalculadoraCustoFCons calculadora = new CalculadoraCustoFCons();
        double valorCusto = 10000 ; 
        double resultado = calculadora.calcular(valorCusto) ; 
        
        assertEquals(17300, resultado , 0.01);
        
    }
    
}
